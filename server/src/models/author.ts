import mongoose, { Model, Document } from 'mongoose';
const mongoosePaginate = require('mongoose-paginate-v2');

const Book = require('./book');

// document
export interface IAuthorDocument extends Document {
    name: String,
    bio: String,
    imagePath: String,
    nationality: any[],
    born: Number,
    died: Number,
    books: any[],
}
// model
export interface IAuthorModel extends Model<IAuthorDocument> {
    // here we decalre statics
    getAuthorById(authorId: String) : Promise<IAuthorDocument>
    addAuthorsBook(targetAuthor:any, bookId:any): Promise<IAuthorDocument>
    getAllAuthors(): Promise<Array<IAuthorDocument>>
    addAuthor(name: String, bio:String, imagePath:String, nationality:any[], born: Number, died: Number ): Promise<IAuthorDocument>
}

const authorSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    bio: {
        type: String,
        required: true,
        default : "unknown"
    },
    imagePath: {
        type: String
    },
    nationality: {
        type: Array,
        required: true,
        default : "unknown"
    },
    born: {
        type: Number,
        required: true
    },
    died: {
        type: Number,
        // required: true,
        default: 0
    },
    books: [{type:mongoose.Schema.Types.ObjectId,
        ref: 'Book'}],
});

authorSchema.index({ name: 'text', bio: 'text' });
authorSchema.plugin(mongoosePaginate);

export interface AuthorSearchI{
    name?: string;
    nationality?: string[];
}

authorSchema.statics.getAuthorById = async function(authorId: String) {
    const author = await this.findById(authorId).populate('books');
    return author ? Promise.resolve(author) : Promise.reject();
}

authorSchema.statics.addAuthorsBook = async function(targetAuthor:any, bookId:any){
    const author = await this.findById(targetAuthor);
    const books = author.books;
    books.push(bookId);
    this.updateOne({ _id: targetAuthor }, { $set: { "books": books } }).exec();
    return author ? Promise.resolve(author) : Promise.reject();
}

authorSchema.statics.getAllAuthors = async function() {
    const authors = await this.find({}, { description: 0})
        //.populate('author', 'name')
        .exec();
    return authors ? Promise.resolve(authors) : Promise.reject();
};


authorSchema.statics.addAuthor = async function(name, bio, imagePath, nationality, born, died) {
    const author = new this({
        'name' : name,
        'bio' : bio,
        'imagePath' : imagePath,
        'nationality' : nationality,
        'born' : born,
        'died' : died
    });
    const savedAuthor = await author.save();
    return savedAuthor;
};
export const AUTHOR_COLL_NAME = 'Author';
const Author : IAuthorModel = mongoose.model<IAuthorDocument, IAuthorModel>(AUTHOR_COLL_NAME, authorSchema);
// const Author = mongoose.model('Author', authorSchema);

export async function searchAuthors(authorCriteria: AuthorSearchI){
    let result: Array<object> = [];
    let criteriaOptions = {};
    if(authorCriteria.name){
        criteriaOptions = {"name": {"$regex": String(authorCriteria.name), "$options":"i"}}
    }
    if(authorCriteria.nationality && authorCriteria.nationality.length !== 0){
        criteriaOptions = {...criteriaOptions, nationality: {$in : authorCriteria.nationality}}; // radi kad se prosledi samo jedna nacionalnost
    }
    (await Author.find(criteriaOptions).exec()).forEach(author=>{
        result.push(author);
    });
    return result;
}


export async function simpleSearchAuthor(keywords: string){
    let result: Array<object> = [];
    const indexName = "a_ss_name_bio";
    let indexes:object = await Author.collection.getIndexes();

    if(!Object.keys(indexes).includes(indexName)){
        await Author.collection.createIndex({
            name: "text",
            bio: "text"
        }, {
            name: "a_ss_name_bio"
        });
    }
    (await Author.find({$text: {$search: keywords}})).forEach(author=>{
        result.push(author);
    });
    return result;
}

export default Author;
