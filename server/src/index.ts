import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';
// import exphbs from 'express-handlebars';
import dotenv from 'dotenv';
import morgan from 'morgan';
import passport from 'passport';
import session from 'express-session';
import {json} from 'body-parser';

const authRoutes = require('./routes/auth');
import storyRoutes from './routes/stories';

import connectDB from '../config/db';
import initPassport from '../config/passport';
import userRouter from './routes/user';
import addBookRouter from './routes/addBook';
import bookRouter from './routes/book';
import reviewRouter from './routes/review';
import authorRouter from './routes/author';
import booksRouter from './routes/books';
import addAuthorRouter from './routes/addAuthor';
import searchRouter from './routes/search';

const MongoStore = require('connect-mongo')(session)

//load config
dotenv.config( {path: '../config/config.env'} );

//passport config
initPassport(passport);

//connecting
connectDB();

const PORT = process.env.port || 3000;
const app = express();

//body parser middleware
app.use(express.urlencoded({ extended: false }))
app.use(express.json())


//Morgan logger
app.use(morgan('dev'))


//Session
app.use(session({
    secret: 'sadasdas2314fsdf32fef3fsdf3f',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: mongoose.connection })
}))


app.use(json());

//CORS
app.use(cors())

//passport middleware
app.use(passport.initialize())
app.use(passport.session())

//set global var
app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

    res.locals.user = req.user || null
    next()
})

//Static-folder
app.use('/uploads', express.static('uploads'));

//Routes
app.use('/auth', authRoutes) // TODO: ovo bi mozda.. trebalo da se drugacije odradi.
app.use('/stories', storyRoutes);
app.use('/users', userRouter);
app.use('/addBook', addBookRouter);
app.use('/book', bookRouter);
app.use('/review', reviewRouter);
app.use('/addAuthor', addAuthorRouter);
app.use('/author', authorRouter);
app.use('/books', booksRouter);
app.use('/search', searchRouter);

app.listen(PORT, ()=>{
    console.log(`server listening on port ${PORT}`);
});


//Obrada gresaka
app.use(function (error: any, req: any, res: any, next: any) {
    console.error(error.stack);
  
    const statusCode = error.status || 500;
    res.status(statusCode).json({
      message: error.message,
      status: statusCode,
      stack: error.stack,
    });
  });
  