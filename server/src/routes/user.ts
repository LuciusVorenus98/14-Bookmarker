import { Router, Request } from "express";
import User from '../models/user';
import { StatusCodes } from 'http-status-codes';
import multer from "multer";
const jwt = require('jsonwebtoken');
let multerFile: Express.Multer.File;

const userRouter = Router();

const storage = multer.diskStorage({
    destination: function (req: Request, file: Express.Multer.File, callback: (error: Error | null, destination: string) => void) {
     callback(null, './uploads/');
 },
 filename: function (req: Request, file: Express.Multer.File, callback: (error: Error | null, filename: string) => void) {
    callback(null, file.originalname);
 }
});

const upload = multer({storage: storage});

userRouter.get('/:idUser', async (req, res) => {
    try {
        const idUser = req.params.idUser;
        const user = await User.getUserById(idUser);
        res.status(StatusCodes.OK).json(user);
    } catch (err) {
        console.error("[err while geting user]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }

});

userRouter.get('/', async (req, res) => {
    try {
        const users = await User.find({}).exec();
        res.status(StatusCodes.OK).json(users);
    } catch (err) {
        console.error("[err while geting user]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }

});

userRouter.patch('/follow', async (req, res) => {
    
        const idUser = req.body.idUser;
        const idFollow = req.body.idFollow;

        const jwt = await User.follow(idUser, idFollow);
        res.status(StatusCodes.OK).json({
            token: jwt});
        
});

userRouter.patch('/unfollow', async (req, res) => {

    const idUser = req.body.idUser;
    const idUnfollow = req.body.idUnfollow;

    const jwt = await User.unfollow(idUser, idUnfollow);
    res.status(StatusCodes.OK).json({
        token: jwt
    });
});

userRouter.patch('/', async(req,res) =>{

    const username = req.body.username;
    const name = req.body.name;
    const surname = req.body.surname;
    const email = req.body.email;
    const password = req.body.password;
    const age = req.body.age;
    const place = req.body.place;

    const jwt = await User.updateUserData(username,name,surname,email,password,age,place);
    res.status(StatusCodes.OK).json({
        token: jwt
    })
});

userRouter.patch('/profile-image', upload.single('image'), async(req,res) => {

const username = req.body.username;

   const imgUrl = req.file.path;
   await User.changeUserProfileImage(username, imgUrl);

   const jwt = await User.getUserJWTByUsername(username);

    res.status(StatusCodes.OK).json({
        token: jwt
    })
})

export default userRouter;