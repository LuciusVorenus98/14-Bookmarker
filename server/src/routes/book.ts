import { Router } from "express";
import Book from '../models/book';
import { StatusCodes } from 'http-status-codes';
const bookRouter = Router();

bookRouter.get('/:id', async (req,res) => {
    try{
        const book = await Book.getBookById(req.params.id);
        res.status(StatusCodes.OK).json(book);
        debugger
    }
    catch(err){
        console.error("[err while geting book]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

bookRouter.get('/', async (req,res) => {
    try{
        const books = await Book.getAllBooks();
        res.status(StatusCodes.OK).json(books);
    }
    catch(err){
        console.error("[err while geting all stories]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

export default bookRouter;