import { Router, Request } from "express";
// const Book = require('../models/book');
import Book from '../models/book';
import { StatusCodes } from 'http-status-codes';
import multer from "multer";
import { ensureAdmin } from "../middleware/auth";
let multerFile: Express.Multer.File;

const addBookRouter = Router();

const storage = multer.diskStorage({
    destination: function (req: Request, file: Express.Multer.File, callback: (error: Error | null, destination: string) => void) {
     callback(null, './uploads/');
 },
 filename: function (req: Request, file: Express.Multer.File, callback: (error: Error | null, filename: string) => void) {
    callback(null, file.originalname);
 }
});

const upload = multer({storage: storage});

addBookRouter.get('/', async (req,res) => {
    try{
        const books = await Book.find();
        res.status(StatusCodes.OK).json(books);
    }
    catch(err){
        console.error("[err while geting all stories]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});
// TODO:  ensureAdmin,
addBookRouter.post('/', upload.single('image'), async(req, res, next) => {
    const title = req.body.title;
    const author = req.body.author;
    const genre = req.body.genres;
    const description = req.body.description;
    const coverImagePath = req.file.path;

    try{
        let book = Book.addBook(title, author, genre, description, coverImagePath);
        res.status(StatusCodes.CREATED).send(book); 
    }
    catch(err){
        console.error(err)
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

export default addBookRouter;