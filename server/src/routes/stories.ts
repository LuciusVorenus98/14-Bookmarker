import express, { Request, Response } from 'express';
const storiesRouter = express.Router();
const authentication = require('../middleware/authentication');
import User from '../models/user';

import Story from '../models/story';
import { ensureAuth } from '../middleware/auth';
import { StatusCodes } from 'http-status-codes';

storiesRouter.patch('/like', async (req, res) => {
    const storyId = req.body.storyId;
    const userId = req.body.userId;
    try {

        const story = await Story.like(storyId, userId);
        res.status(StatusCodes.OK).json(story);
    }
    catch (err) {
        console.error("[err while posting a like to story]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

storiesRouter.patch('/dislike', async (req, res) => {
    const storyId = req.body.storyId;
    const userId = req.body.userId;
    try {

        const story = await Story.dislike(storyId, userId);
        res.status(StatusCodes.OK).json(story);
    }
    catch (err) {
        console.error("[err while posting a like to story]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});



//@desc     Process the add form
//@route    POST/stories
//                   ensureAuth,  dodati posle, za sad radi testa neka bude ovako
storiesRouter.post('/add', async (req: Request, res: Response) => {
    const storyTitle = req.body.storyTitle;
    const storyContent = req.body.storyContent;
    const storyAuthor = req.body.storyAuthor;

    try {

        let story = await Story.addStory(storyTitle, storyContent, storyAuthor);
        res.status(StatusCodes.CREATED).json(story);
    }
    catch (err) {
        console.error(err)
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
})



// TODO: da li ovo mogu da vide svi ili i Guest?
//@desc     Show all stories
//@route    GET/stories
//                   ensureAuth,  dodati posle, za sad radi testa neka bude ovako
storiesRouter.get('/following/:idUser', async (req, res) => {
    try {
        const user = await User.getUserById(req.params.idUser);
        const following = user.following;
        const arrayLength = following.length;

        if (arrayLength === 0) {
            console.log("ne prati nikoga");
            res.status(StatusCodes.OK).send([]);
        }
        else {
            const idFirstFollowing = user.following[0];
            const firstFollowing = await User.getUserById(idFirstFollowing);
            // let stories = await Story.findForUser(firstFollowing).populate('user').sort({ createdAt: 'desc' }).lean()
            let stories = await Story.findForUser(firstFollowing);
            //petlja
            for (let i = 1; i < arrayLength; i++) {
                let userWanted = await User.getUserById(following[i]);
                // stories = stories.concat(await Story.findForUser(userWanted).populate('user').sort({ createdAt: 'desc' }).lean());
                stories = stories.concat(await Story.findForUser(userWanted))
            }

            res.status(StatusCodes.OK).send(stories);
        }
    }
    catch (err) {
        console.error("[err while geting all following stories]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

storiesRouter.get('/:idUser', async (req, res) => {
    try {
        const user = await User.getUserById(req.params.idUser);

        let stories = await Story.findForUser(user);

        res.status(StatusCodes.OK).json(stories);
    }
    catch (err) {
        console.error("[err while geting all stories]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});




export default storiesRouter;