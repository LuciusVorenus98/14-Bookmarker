import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user.model';
import { passwordMatch } from './password-match.validator';
import { RegisterService } from './register.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public checkoutForm: FormGroup;
  public submitted: boolean;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) { 

    this.checkoutForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required, passwordMatch()]],
      name: ['', [Validators.required]],
      surname: ['',[Validators.required]],
      email: ['', [Validators.required, Validators.email]]
  });

  this.checkoutForm.controls.password.valueChanges.subscribe(
    x=>this.checkoutForm.controls.confirmPassword.updateValueAndValidity()
  )
      this.submitted = false;
  }

  ngOnInit(): void {

    this.authService.getAllUsers().subscribe(users => {
      console.log(users);
    });

  }

  public username() {
    return this.checkoutForm.get('username');
  }

  public password() {
    return this.checkoutForm.get('password');
  }

  public confirmPassword() {
    return this.checkoutForm.get('confirmPassword');
  }

  public name() {
    return this.checkoutForm.get('name');
  }

  public surname() {
    return this.checkoutForm.get('surname');
  }

  public email() {
    return this.checkoutForm.get('email');
  }

  public submitForm(data: any){
      console.log(data);
      this.submitted = true;

      if(!this.checkoutForm.valid){
        return;
      }

      this.authService.createUser(data).subscribe((user: any) => {
      this.router.navigate(['../login']);
      window.alert('User is successfully created!');
      console.log(user);
    });
  }

}
