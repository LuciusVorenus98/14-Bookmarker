import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { Router, ActivatedRoute } from '@angular/router';
// import { msg } from './login.service'

import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { AuthService, Msg } from '../services/auth.service';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public checkoutForm: FormGroup;
  public submitted: boolean;
  public message: Msg;
  socialUser: SocialUser;
  private sub: Subscription;
  public loggedInUser: User = null;

  constructor(private authService: LoginService,
    private authServ: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private socialAuthService: SocialAuthService) {

    this.sub = this.authServ.user.subscribe((user: User) => {
      this.loggedInUser = user;
    });

    this.authServ.sendUserDataIfExists();

    this.checkoutForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });

    this.submitted = false;

  }

  ngOnInit(): void {}

  loginWithGoogle(): void {
    this.authService.loginWithGoogle();
  }

  logOutGoogle(): void {
    this.authService.logOutGoogle();
  }

  public username() {
    return this.checkoutForm.get('username');
  }

  public password() {
    return this.checkoutForm.get('password');
  }

  public submitForm(data: any){
    console.log(data);

    this.submitted= true;

    if(!this.checkoutForm.valid)
      return;

    this.authServ.loginUser(data).subscribe((user: any) => {

        this.router.navigate(['../dashboard/' + user.id]);
        console.log('Ulogovan korisnik');

        console.log(user);
      
    }, 
    (error: any) => {
      this.message = error.message;
       });
  }

  onSignIn(googleUser: any){
    console.log(googleUser.getBasicProfile());
    

  }


}
