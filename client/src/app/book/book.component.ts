import { Component, OnInit } from '@angular/core';
import { BookService } from './book.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';

declare const M: any;

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})


export class BookComponent implements OnInit {

  private sub: Subscription;
  public loggedInUser: User = null;

  num: any;
  yourRating: any = 0;
  averageRating: any;

  book: any;
  reviews: any;
  paramMapSub: any;
  idBook: any;

  public rateBookForm: FormGroup;
  public checkoutForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private bookService: BookService,
              private route: ActivatedRoute,
              private authService: AuthService) {

    this.rateBookForm = this.formBuilder.group({
      rating: ['']
    })

    this.checkoutForm = this.formBuilder.group({
      title: [''],
      reviewContent: ['']
    });

    this.paramMapSub = this.route.paramMap.subscribe(params => {
      this.idBook = params.get('idBook');

      this.bookService.getBook(this.idBook).subscribe((book: any) => {
        this.book = [book];
      });
    });

    this.sub = this.authService.user.subscribe((user: User) => {
      this.loggedInUser = user;
    });

    this.authService.sendUserDataIfExists();
  }

  ngOnInit(): void {
    this.num = 1;
    //Initialization
    
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems);
    elems = document.querySelectorAll('.collapsible');
    instances = M.Collapsible.init(elems);
    this.showBook();
    
  }

  public submitReview(data: any) {
    if(data.title == "" || data.reviewContent == "")
      return;
    data.userId = this.loggedInUser.id;
    this.bookService.createReview(data, this.idBook).subscribe((data: any) => {
      console.log(data);
    });
    this.checkoutForm.reset();
  }

  public submitRating(data: any) {
    if(data.rating == "")
      return;
    this.yourRating = data.rating;
    data.userId = this.loggedInUser.id;
    this.bookService.createRating(data, this.idBook).subscribe((data: any) => {
      console.log(data);
    });
    this.checkoutForm.reset();
  }

  public showBook() {
    this.bookService.getBookReviews(this.idBook).subscribe((data: any) => {
      this.reviews = data;
      for(var i = 0; i < this.reviews.length; i++){
        if(this.reviews[i].reviews.user._id == this.loggedInUser.id)
          this.yourRating = this.reviews[i].reviews.rating;
      }
      this.getAverageRating();
      this.reviews.sort(function (d1: any, d2: any) {
        var t1 = d1.reviews.postedAt;
        var t2 = d2.reviews.postedAt;
        if (t1 < t2)
          return 1;
        else
          return -1
      });
    });
  }

  public findPath(path: String) {
    return "http://localhost:3000/" + path.replace('\\', '/');
  }

  public addReview() {
    if (this.num < this.reviews.length)
      this.num = this.num + 2;
  }

  public closeReviews() {
    this.num = 1;
  }

  formatDate(date: Date) {
    var date2 = new Date(date)
    var dd = String(date2.getDate()).padStart(2, '0');
    var mm = String(date2.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = date2.getFullYear();

    return dd + '.' + mm + '.' + yyyy;
  }

  public getAverageRating(){
    var num = 0;
    var sum = 0;
    for(var i = 0; i < this.reviews.length; i++){
      if(this.reviews[i].reviews.rating > 0)
        num++;
        sum += this.reviews[i].reviews.rating;
    }
    
    if(num>0)
      this.averageRating = sum/num;
    else
      this.averageRating = 0;
  }

}

