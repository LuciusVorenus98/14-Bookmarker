import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';

declare const M: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  private sub: Subscription;
  public loggedInUser: User = null;
  isAdmin : boolean = false;
  
  constructor(private authService: AuthService) { 
    this.sub = this.authService.user.subscribe((user: User) => {
      this.loggedInUser = user;      
      this.isAdmin= user.role === "admin";
    });

    this.authService.sendUserDataIfExists();
    
  }

  ngOnInit(): void {
    M.Sidenav.init(document.querySelector('.sidenav'));
  }

  public logout(): void {
    this.authService.logoutUser();
  }
}
