import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-edit-profile-formular',
  templateUrl: './edit-profile-formular.component.html',
  styleUrls: ['./edit-profile-formular.component.css']
})
export class EditProfileFormularComponent implements OnInit {
  
  public checkoutForm: FormGroup;
  imageForUpload: any = null;
  private sub: Subscription;
  public loggedInUser: User = null;

  constructor(private formBuilder: FormBuilder,
             private userService: UserService,
             private authService: AuthService) {


    this.sub = this.authService.user.subscribe((user: User) => {
      this.loggedInUser = user;

      this.checkoutForm = this.formBuilder.group({
        name: [user.name],
        surname: [user.surname],
        email: [user.email],
        password: [''],
        imgUrl: [user.image],
        age: [user.age],
        place: [user.place]
      });
    });

    this.authService.sendUserDataIfExists();

    console.log(this.loggedInUser)
   }

  ngOnInit(): void {
  }

  public submitForm(data: any) {
  
    data['username'] = this.loggedInUser.username;

    this.userService.patchUserData(data).subscribe((user: any) => {

      if (this.imageForUpload !== null) {
        this.userService.patchUserProfileImage(this.imageForUpload, this.loggedInUser.username).subscribe((user: User) => {})
      }

      this.checkoutForm.reset();
      window.location.reload();
      console.log(user);
    })

  }

  public selectImage(event: Event) {
    const files: FileList = (event.target as HTMLInputElement).files;
    if (files.length === 0) {
      this.imageForUpload = null;
      return;
    }
    this.imageForUpload = files[0];

  }

}
