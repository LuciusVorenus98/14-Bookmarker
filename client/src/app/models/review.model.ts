export interface BookReview{ 
    book: string, // id
    reviews: {
        user: string, // id
        rating: Number,
        postedAt: Date,
        title: String,
        comment: String
    }
}