import { FilterField } from "../search/filter-field/filter-field.component";

export interface StorySearchI {
    title?: string;
    fromDate?: string;
    toDate?: string; // 2015-07-07T00:00:00.000Z format svih datuma
    keywords?: string[];
}

export function getStorySearchStrings(): FilterField[] {
    return [
        { fieldName: "title", fieldTitle: "Story title", type: "text"},
        { fieldName: "fromDate", fieldTitle: "From date", type:"datetime"},
        { fieldName: "toDate", fieldTitle: "To date", type: "datetime"},
        { fieldName: "keywords", fieldTitle: "Keywords", type: "list" },
    ];
}

export interface UserSearchI{
    name?: string;
    username?: string;
    surname?: string;
}

export function getUserSearchStrings(): FilterField[] {
    return [
        { fieldName: "name", fieldTitle: "Name", type: "text" },
        { fieldName: "surname", fieldTitle: "Surname" , type: "text"},
        { fieldName: "username", fieldTitle: "Username", type: "text" },
    ];
}

export interface BookSearchI{
    title?: string;
    author?: string;
    genre?: string[];
    keywords?: string[];
}

export function getBookSearchStrings(): FilterField[] {
    return [
        { fieldName: "title", fieldTitle: "Book title" , type: "text"},
        { fieldName: "author", fieldTitle: "Authors name", type: "text" },
        { fieldName: "genre", fieldTitle: "Genre(s)", type: "list" },
        { fieldName: "keywords", fieldTitle: "Keywords", type: "list" }
    ];
}

export interface AuthorSearchI{
    name?: string;
    nationality?: string[];
}
export function getAuthorSearchStrings(): FilterField[] {
    return [
        { fieldName: "name", fieldTitle: "Name", type: "text" },
        { fieldName: "nationality", fieldTitle: "Nationality" , type: "list"}
    ];
}

/** Koristi se za slanje zahteva serveru kao body */
export interface SearchI {
    user?: UserSearchI;
    book?: BookSearchI;
    author?: AuthorSearchI;
    story?: StorySearchI;
    review?: ReviewSearchI;
    simpleSearch?: SimpleSearchI;
}

export enum SearchType {
    USER='User',
    STORY= 'Story',
    BOOK ='Book',
    AUTHOR='Author',
    REVIEW= 'Review',
    SIMPLE = 'All'
} 

export function getSearchType(searchTypeValue: string): SearchType | undefined{
    if (searchTypeValue === SearchType.USER) {
        return SearchType.USER;
    }
    else if (searchTypeValue === SearchType.AUTHOR) {
        return SearchType.AUTHOR;
    }
    else if (searchTypeValue === SearchType.BOOK) {
        return SearchType.BOOK;
    }
    else if (searchTypeValue === SearchType.STORY) {
        return SearchType.STORY;
    }
    else if (searchTypeValue === SearchType.REVIEW) {
        return SearchType.REVIEW;
    }
    else if (searchTypeValue === SearchType.SIMPLE) {
        return SearchType.SIMPLE;
    }
    return undefined;
}

export function getSearchWrapperNameFor(searchType: SearchType):string|undefined{
    if (searchType === SearchType.USER) {
        return "user";
    }
    else if (searchType === SearchType.AUTHOR) {
        return "author";
    }
    else if (searchType === SearchType.BOOK) {
        return "book";
    }
    else if (searchType === SearchType.STORY) {
        return "story";
    }
    else if (searchType === SearchType.REVIEW) {
        return "review";
    }
    else if (searchType === SearchType.SIMPLE) {
        return "simpleSearch";
    }
    return undefined;
}

export interface ReviewSearchI{
    book?: string,
    user?: string,
    title?: string,
    fromDate?: string,
    toDate?: string,
    keywords?: string[],
}

export function getReviewSearchStrings(): FilterField[] {
    return [
        { fieldName: "book", fieldTitle: "Book title", type: "text" },
        { fieldName: "user", fieldTitle: "Users name", type: "text" },
        { fieldName: "title", fieldTitle: "Review title", type: "text" },
        { fieldName: "fromDate", fieldTitle: "From date", type: "datetime" },
        { fieldName: "toDate", fieldTitle: "To date", type: "datetime" },
        { fieldName: "keywords", fieldTitle: "Keywords" , type: "list"}
    ];
}

export interface SimpleSearchI {
    keywords?: string[],
}

