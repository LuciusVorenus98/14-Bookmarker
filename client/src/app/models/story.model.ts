export interface Story{
    title: string;
    body: string;
    user: any;
    createdAt: Date;
    usersThatLikeStory: Array<any>;
}